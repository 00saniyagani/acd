package kz.aitu.week1;

import java.util.Scanner;

public class Practice7 {

    int a[][] = new int[5][5];
    public void inputData() {
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < 5; i ++) {
            for (int j = 0; j < 5; j ++) {
                a[i][j] = scanner.nextInt();
            }
        }
    }

    public void print() {
        for (int i = 0; i < 5; i ++) {
            for (int j = 0; j < 5; j ++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }

    public boolean findPath(int i, int j) {
        //write your code here
        return true;
    }

    public void run() {
        inputData();
        //System.out.println(findPath(0, 0));
        print();
    }
}
