package kz.aitu.week_3.ls_cs03;

import lombok.Data;

@Data
public class LinkedList {
    private Node left;
    private Node right;

    public void addNode(Node node) {
        //push right code  and get 50points for week3
        left = node;
        right = null;
    }

    public void print() {

        Node node = left;
        while(node != null) {
            System.out.println(node.getValue());
            node = node.getNextNode();
        }
    }
}
